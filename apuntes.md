# Apuntes de CSS
**Apuntes hechos para mí, estoy aprendiendo :)**

### Elementos inline y block

>Inline no producen salto de linea, block sí.





En css se escoge los elementos a los que se le agrega el estilo, así.
### clases
```
.clase {
 propiedad:valor;
}
```
clase puede ser una clase asignada a cualquier elemento de HTML (dev, p, span, etc.)

### Identificador (id en html)
```
#Identificador {
 propiedad:valor;
}
```
### Elementos  ya tipados por HTML
```
dev {
 propiedad:valor;
}
```
## Propiedades aprendidas
```
ejemplo{
  background:color;
  border: propiedades;
  display: valores;
}

```
# Bootstrap
### Grillas, flexbox y containers


Grid nos ayuda a organizar elementos en dos elementos, mientras que flexbox en una dimensión.

flexbox
```
_____________________
|     |      |      |
|     |      |      |
|     |      |      |
|     |      |      |
|     |      |      |
|     |      |      |
```
Grid
```
_____________________
|_____|______|_______|
|_____|______|_______|
|_____|______|_______|
|_____|______|_______|
|_____|______|_______|
```
#### Atributo flex-direction
está ___row___ que es filas y ___colunms___, columnas.
```
flex-direction: row; (row-reverse, column-reverse)
```
#### Algunos atributos que cambian


`display: block;`  *esto lo convierte en un block si es inline*

`display: none;` desaparece el elemento.

`position: fixed;` posición fija a un lugar de la pantalla

`position: absolute;` posición fija a un lugar del elemento padre.


##### Ejemplo de posicionamiento
Esto sería arriba a la izquierda:
```
dev{
  position:absolute;
  top:0;
  left:0;
}
```

#### Bootstrap en HTML
Utilizamos **class** en los atributos del elemento en html para darle forma.


__Ejemplo__

```
<div class="container">
  <div class="row">
    <div class="col-sm"><h2>Columna de ejemplo 1</h2>Sí las uvas pasas.</div>
    <div class="col-sm"><h2>Columna de ejemplo 2</h2>Sí las uvas pasas.</div>
    <div class="col-sm"><h2>Columna de ejemplo 3</h2>Sí las uvas pasas.</div>

  </div>

</div>

```
En este ejemplo vemos un `container` con una `row` dentro la cual tiene columnas (`col-sm`)


`col-sm` organiza las columnas de forma proporcional, tres columnas, cada una ocupa 30% del `row`.

`col-sm` significa small. También está  `col-lg`, large y `col-md`, medium.

Podría haber usado `col` que se ajusta a la pantalla.

##### Otro ejemplo con offset
`col-sm offset-sm-4` esto corre una columna 4 hacia la izquierda, recuerda que en bootstrap son 12 col por defecto.
```
  <div class="row">
    <div class="col-sm"><h2>Columna de ejemplo 1</h2>Sí las uvas pasas.</div>
    <div class="col-sm-2 offset-sm-4"><h2>Columna de ejemplo 2</h2>Sí las uvas pasas.</div>
    <div class="col-sm"><h2>Columna de ejemplo 3</h2>Sí las uvas pasas.</div>
  </div>

```
Hay dos clases, una para definir el ancho y la otra para el offset o corrimiento.

`Curso.Prep.Henry`
